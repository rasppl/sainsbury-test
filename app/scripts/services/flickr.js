'use strict';

/**
 * @ngdoc service
 * @name sainsburyTestApp.Flickr
 * @description
 * # Flickr
 * Factory in the sainsburyTestApp.
 */
angular.module('sainsburyTestApp')
    .factory('Flickr', function ($resource) {
        return $resource('http://api.flickr.com/services/feeds/photos_public.gne', { format: 'json', jsoncallback: 'JSON_CALLBACK' }, { 'load': { 'method': 'JSONP' } });
    })
;
