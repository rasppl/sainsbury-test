'use strict';

/**
 * @ngdoc directive
 * @name sainsburyTestApp.directive:Masonry
 * @description
 * # Masonry
 */
angular.module('sainsburyTestApp')
    .directive('ngLike', function () {
        return {
            restrict: 'C',
            link: function (scope) {
                scope.liked = function() {
                    if (scope.likes.indexOf(scope.item.link) > -1) {
                        return true;
                    } else {
                        return false;
                    }
                };
            }
        };
    })
;
