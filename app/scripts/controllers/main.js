'use strict';

/**
 * @ngdoc function
 * @name sainsburyTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sainsburyTestApp
 */
angular.module('sainsburyTestApp')
    .controller('MainCtrl', ['$scope', '$localStorage', 'Flickr', function ($scope, $localStorage, Flickr) {

        $scope.stream = Flickr.load({ tags: 'london' });

        $localStorage.$default({
            likes: []
        });

        $scope.likes = $localStorage.likes;

        $scope.like = function(item) {
            $scope.likes.push(item.link);
        };

        $scope.unline = function(item) {
            $scope.likes.splice($scope.likes.indexOf(item.link), 1);
        };

     }])
;
