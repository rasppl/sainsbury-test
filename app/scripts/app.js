'use strict';

/**
 * @ngdoc overview
 * @name sainsburyTestApp
 * @description
 * # sainsburyTestApp
 *
 * Main module of the application.
 */
angular
    .module('sainsburyTestApp', [
        'ngResource',
        'ngStorage'
    ])
;
